variables:
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_DRIVER: overlay2
  CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX: forgemia.inra.fr:443/game-theory-tools-group/dependency_proxy/containers
  CONTAINER_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
  REF_BASE_IMAGE: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/sagemath/sagemath:9.0-py3
  DOCKER_BUILDKIT: 1
  GIT_DEPTH: 1

linter_flake8:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.8.10
  script:
    - pip install "flake8>=3.7.0"
    - flake8 --output-file "flake8-report.txt"
  after_script:
    - python -m pip install "flake8-junit-report"
    - python -m junit_conversor "flake8-report.txt" "junit.xml"
  artifacts:
    when: always
    paths: 
      - junit.xml
      - flake8-report.txt
    reports:
      junit: junit.xml

linter_dockerfile:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/hadolint/hadolint:2.8.0-alpine
  variables:
    files_to_lint: ""
  script: |
    mkdir -p reports
    hadolint -f gitlab_codeclimate docker/Dockerfile > reports/hadolint-$(md5sum docker/Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths: 
      - "reports/*"
    when: always

build_docker:
  tags: 
    - stable
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_DEPENDENCY_PROXY_USER -p $CI_DEPENDENCY_PROXY_PASSWORD $CI_DEPENDENCY_PROXY_SERVER
  script:
    - docker build 
        --cache-from $CONTAINER_IMAGE 
        --tag $CONTAINER_IMAGE:latest 
        --tag $CONTAINER_IMAGE:$CI_COMMIT_SHA 
        --file docker/Dockerfile
        --build-arg BUILDKIT_INLINE_CACHE=1
        --build-arg REGISTRY_DOMAIN=${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}
        "."
  after_script:
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker push $CONTAINER_IMAGE:latest
    - docker push $CONTAINER_IMAGE:$CI_COMMIT_SHA
    - docker logout "${CI_REGISTRY}"


test_pytest:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:stable
  services:
    - docker:dind
  variables:
    TEST_DIR: "test"
  before_script:
    - docker info
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker pull $CONTAINER_IMAGE:$CI_COMMIT_SHA
  script:
    - docker run -v "$(pwd):/mnt" -w /mnt/$TEST_DIR $CONTAINER_IMAGE:$CI_COMMIT_SHA "pytest --junitxml=report.xml"
  after_script:
    - docker logout "${CI_REGISTRY}"
  artifacts:
    when: always
    paths: 
      - "$TEST_DIR/report.xml"
    reports:
      junit: "$TEST_DIR/report.xml"
  needs: ["build_docker"]
  

export_jupyternb:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:stable
  services:
    - docker:dind
  variables:
    JUPYTERNB_FOLDER: "./notebook/"
    JUPYTERNB_FILE: "*"
  before_script:
    - docker info
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker pull $CONTAINER_IMAGE
  script:
    - docker run -v "$(pwd):/mnt" -w /mnt $CONTAINER_IMAGE:$CI_COMMIT_SHA "ls -1 ${JUPYTERNB_FOLDER}${JUPYTERNB_FILE}.ipynb | xargs jupyter nbconvert --ExecutePreprocessor.timeout=600 --to html --execute"
  after_script:
    - docker logout "${CI_REGISTRY}"
  artifacts:
    paths:
      - ${JUPYTERNB_FOLDER}${JUPYTERNB_FILE}.html
  needs: ["build_docker"]

pages:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.7-alpine
  variables:
    GIT_DEPTH: 2147483647
  script:
  - pip install -U sphinx
  - pip install sphinx-rtd-theme
  - apk add git
  - pip install sphinx-multiversion gitpython
  - sphinx-apidoc -o docs/source gtnash
  - sphinx-multiversion docs/source public
#  - sphinx-build -b html docs/source public
  artifacts:
    paths:
    - public


deploy_pypi:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.8.10
  script:
  - pip install twine
  - python setup.py sdist bdist_wheel
  - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --verbose --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  rules:
    - changes:
      - setup.py
  when: manual
