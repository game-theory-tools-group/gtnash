.. GTNash documentation master file, created by
   sphinx-quickstart on Thu Feb  3 13:20:54 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GTNash's documentation!
==================================

.. toctree::
   maxdepth: 2
   caption: Contents:
   ./introduction.rst
   ./quickstart.rst
   ./gameclasses.rst
   ./solvers.rst
   ./utilities.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
