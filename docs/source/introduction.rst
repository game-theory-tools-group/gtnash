Introduction
============

This is the developper's guide to the **GTNash** Python/Sagemath toolbox.

The **GTNash** toolbox proposes a set of tools to represent games in (i) normal form (ii) succinct (polymatrix, graphical, hypergraphical) form (iii) succinct form with incomplete information (bayesian, bayesian hypergraphical).

The **GTNash** toolbox also proposes procedures to compute pure Nash equilibira through enumeration and mixed Nash equilibria in normal-form games (Porter, Nudelman and Shoam's algorithm), polymatrix games (Lemke-Howson algorithm) and hypergraphical game (including complete /incomplete information games) by a algorithm extending Wilson's algorithm.

Please refer to [FJS2022] for a description of the algorithm. Also use [FJS2022] if you wish to cite the software.

In normal use, *GTNash* is intended to be used as a Python package, called from and used within the `Sagemath <https://www.sagemath.org/>`_ software system.
All sources and a Sagemath/Jupyter Quickstart notebook are available in the `gtnash <https://forgemia.inra.fr/game-theory-tools-group/gtnash/>`_ project repository.

.. [FJS2022] Fargier, H., Jourdan, P. and R. Sabbadin. A Path-following Polynomial Equations Systems Approach for Computing Nash Equilibria. *Proc' of AAMAS*, 2022.
