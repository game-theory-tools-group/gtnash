The different solvers
=====================

Three different solvers for N-players games are currently implemented:

* Wilson's algorithm (normal-form and hypergraphical games).
* Porter, Nudelman and Shoam's algorithm (normal-form games).
* Howson's algorithm (polymatrix games)

Wilson's algorithm
------------------

.. automodule:: wilsonalgorithm
   :members:

Porter, Nudelman and Shoam's algorithm
--------------------------------------

.. automodule:: pns
   :members:

Howson's algorithm
------------------

.. automodule:: lemkehowson_polymatrix
   :members:
