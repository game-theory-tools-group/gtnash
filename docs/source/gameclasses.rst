The different classes of games
==============================

The following game classes are implemented:

* ``abstractgame``: An abstract class from which all other game classes inherit.
* ``normalformgame``: The class of normal form games.
* ``polymatrixgame``: The class of polymatrix games.
* ``hypergraphicalgame``: The clas of hypergraphical games.
* ``bayesiangame``: The class of bayesian games.
* ``bayesian_hypergraphicalgame``: The class of bayesian hypergraphical games.

These classes are documented here:

Abstract games class
--------------------

.. automodule:: abstractgame
   :members:

Normal form games class
-----------------------

.. automodule:: normalformgame
   :members:


Polymatrix games class
----------------------

.. automodule:: polymatrixgame
   :members:


Hypergraphical games class
--------------------------

.. automodule:: hypergraphicalgame
   :members:


Bayesian games class
--------------------

.. automodule:: bayesiangame
   :members:


Bayesian hypergraphical games class
-----------------------------------

.. automodule:: bayesian_hypergraphicalgame
   :members:

