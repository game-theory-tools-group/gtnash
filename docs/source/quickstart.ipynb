{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# gtnash Tutorial\n",
    "Illustration of the functionalities of the gtnash module.\n",
    "\n",
    "Before executing this notebook, you should open a jupyter notebook under Python 3.0 (instead of Sagemath 9.0) in the gtnash directory and run:\n",
    "pip install -e .\n",
    "\n",
    "Then, you need to import the following packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import gtnash\n",
    "from sage.all import *\n",
    "from gtnash.game.normalformgame  import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Normal form games\n",
    "A normal-form game can be built from (i) a list of lists of available actions for every player and (ii) a list of lists of utilities for every player (one utility for each joint action).\n",
    "\n",
    "### 1.1 Defining a NFG object\n",
    "A NFG can be built from a Python's list of lists. Alternately, .nfg format (Gambit's format: http://gambit.sourceforge.net/ ) ascii files can be read and written."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Utilities of the game\n",
      "[['A0' 'A1' 'A2' 'U0' 'U1' 'U2']\n",
      " ['0' '0' '0' '6' '0' '4']\n",
      " ['0' '0' '1' '7' '3' '7']\n",
      " ['0' '1' '0' '0' '7' '4']\n",
      " ['0' '1' '1' '5' '4' '0']\n",
      " ['1' '0' '0' '1' '6' '3']\n",
      " ['1' '0' '1' '4' '5' '2']\n",
      " ['1' '1' '0' '2' '1' '6']\n",
      " ['1' '1' '1' '3' '2' '1']]\n"
     ]
    }
   ],
   "source": [
    "players_actions = [[0, 1], [0, 1], [0, 1]]\n",
    "utilities=[[6,7,0,5,1,4,2,3],[0,3,7,4,6,5,1,2],[4,7,4,0,3,2,6,1]]\n",
    "current_nfg=NFG(players_actions,utilities)\n",
    "print(\"Utilities of the game\")\n",
    "print(current_nfg)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Solving using Wilson's algorithm\n",
    "This uses the implementation of Wilson's method:\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "Robert Wilson. 1971. Computing equilibria of N-person games. SIAM J. Appl. Math.\n",
    "21, 1 (1971), 80–87.\n",
    "</div>\n",
    "\n",
    "described here:\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "Hélène Fargier, Paul Jourdan and Régis Sabbadin. A path-following polynomial equations systems approach for\n",
    "computing Nash equilibria. AAMAS, 2022.\n",
    "</div>\n",
    "\n",
    "and which code can be found there:\n",
    "\n",
    "<div class=\"alert alert-block alert-warning\">\n",
    "Put Git repository and/or webpage address here.\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gtnash.solver.wilsonalgorithm import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Applying Wilson's algorithm returns allows to compute a mixed Nash equilibrium:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{0: [0.4166666666666667?, 0.5833333333333333?], 1: [0.2857142857142857?, 0.714285714285715?], 2: [1.000000000000000?, 0]}\n",
      "Is this an equilibrium?  True\n"
     ]
    }
   ],
   "source": [
    "equilibrium = wilson(current_nfg)\n",
    "print(equilibrium)\n",
    "print(\"Is this an equilibrium? \",current_nfg.is_equilibrium(equilibrium))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Remark\n",
    "These coordinates are actually not an approximation of the equilibrium, but are exact \"algebraic numbers\", which radical expression (exact representations in terms of single variable polynomials) can be recovered.\n",
    "In the present example, the equilibrium coordinates are rational numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Player 0: [5/12, 7/12]\n",
      "Player 1: [2/7, 5/7]\n",
      "Player 2: [1, 0]\n"
     ]
    }
   ],
   "source": [
    "for n in equilibrium.keys():\n",
    "    print(f\"Player {n}: {[x.radical_expression() for x in equilibrium[n]]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Returning a Nash equilibrium using Wilson's algorithm, in one command"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{0: [0.4166666666666667?, 0.5833333333333333?], 1: [0.2857142857142857?, 0.714285714285715?], 2: [1.000000000000000?, 0]}\n",
      "CPU times: user 784 ms, sys: 16 ms, total: 800 ms\n",
      "Wall time: 798 ms\n"
     ]
    }
   ],
   "source": [
    "%time print(wilson(current_nfg))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3 Solving using Porter, Nudelman and Shoam's algorithm\n",
    "We can solve the same game using Porter, Nudelman and Shoam's algorithm.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "Ryan Porter and Eugene Nudelman and Yoav Shoham.\n",
    "Simple Search Methods for Finding a Nash Equilibrium. \n",
    "    <i> AAAI </i>, \n",
    "2004.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{0: [0.4166666666666667?, 0.5833333333333333?], 1: [0.2857142857142857?, 0.714285714285715?], 2: [1.000000000000000?, 0]}\n",
      "CPU times: user 186 ms, sys: 16.4 ms, total: 203 ms\n",
      "Wall time: 330 ms\n"
     ]
    }
   ],
   "source": [
    "from gtnash.solver.pns import *\n",
    "%time print(PNS_solver(current_nfg).launch_pns())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Polymatrix games\n",
    "\n",
    "### 2.1 Definition of a polymatrix game\n",
    "Definition of a polymatrix game: Object, ascii representation...\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "Elena B. Yanovskaya. 1968. Equilibrium points in polymatrix games. Litovskii\n",
    "Matematicheskii Sbornik 8 (1968), 381–384.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gtnash.game.polymatrixgame import *\n",
    "from gtnash.solver.lemkehowson_polymatrix import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a polymatrix game with three players and three actions each:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Utilities of the game\n",
      "Local game  0 : \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '8' '4']\n",
      " ['0' '1' '2' '1']\n",
      " ['0' '2' '5' '4']\n",
      " ['1' '0' '5' '5']\n",
      " ['1' '1' '1' '6']\n",
      " ['1' '2' '7' '2']\n",
      " ['2' '0' '1' '4']\n",
      " ['2' '1' '5' '7']\n",
      " ['2' '2' '1' '3']]\n",
      "Local game  1 : \n",
      " [['A0' 'A2' 'U0' 'U2']\n",
      " ['0' '0' '2' '2']\n",
      " ['0' '1' '2' '5']\n",
      " ['0' '2' '4' '4']\n",
      " ['1' '0' '7' '7']\n",
      " ['1' '1' '9' '1']\n",
      " ['1' '2' '4' '5']\n",
      " ['2' '0' '5' '2']\n",
      " ['2' '1' '5' '1']\n",
      " ['2' '2' '5' '6']]\n",
      "Local game  2 : \n",
      " [['A1' 'A2' 'U1' 'U2']\n",
      " ['0' '0' '9' '8']\n",
      " ['0' '1' '3' '8']\n",
      " ['0' '2' '4' '6']\n",
      " ['1' '0' '9' '9']\n",
      " ['1' '1' '5' '1']\n",
      " ['1' '2' '5' '4']\n",
      " ['2' '0' '9' '9']\n",
      " ['2' '1' '5' '1']\n",
      " ['2' '2' '4' '3']]\n",
      "\n",
      " Hypergraph: \n",
      "[[0, 1], [0, 2], [1, 2]]\n"
     ]
    }
   ],
   "source": [
    "utilities = [[[8, 2, 5, 5, 1, 7, 1, 5, 1], [4, 1, 4, 5, 6, 2, 4, 7, 3]],\n",
    "             [[2, 2, 4, 7, 9, 4, 5, 5, 5], [2, 5, 4, 7, 1, 5, 2, 1, 6]],\n",
    "             [[9, 3, 4, 9, 5, 5, 9, 5, 4], [8, 8, 6, 9, 1, 4, 9, 1, 3]]]\n",
    "hypergraph = [[0, 1], [0, 2], [1, 2]]\n",
    "players_actions = [[0, 1, 2], [0, 1, 2], [0, 1, 2]]\n",
    "\n",
    "current_pmg=PMG(players_actions,utilities,hypergraph)\n",
    "print(\"Utilities of the game\")\n",
    "print(current_pmg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Solving a polymatrix game using the Lemke-Howson algorithm\n",
    "More precisely, this is an implementation of:\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "Joseph T. Howson.\n",
    "Equilibria of polymatrix games.\n",
    "<i> Management Science </i>, \n",
    "18(5):pp. 312–318, 1972.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 4.33 ms, sys: 19 µs, total: 4.35 ms\n",
      "Wall time: 3.94 ms\n",
      "player 0 -> mixed strategy -> [0.0, 0.0, 1.0]\n",
      "player 1 -> mixed strategy -> [0.0, 1.0, 0.0]\n",
      "player 2 -> mixed strategy -> [1.0, 0.0, 0.0]\n"
     ]
    }
   ],
   "source": [
    "solver = LHpolymatrix(PMG(players_actions, utilities, hypergraph))\n",
    "omega0 = [0,0,0] # Initial joint action used to initialize the Lemke-Howson algorithm\n",
    "%time sol_dict = solver.launch_solver(omega0)\n",
    "for key in sol_dict.keys():\n",
    "    mixed_strat = [float(p) for p in sol_dict[key]]\n",
    "    print(f'player {key} -> mixed strategy -> {mixed_strat}')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check that this mixed strategy is a Nash equilibrium:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Is it a Nash equilibrium?  True\n"
     ]
    }
   ],
   "source": [
    "print(\"Is it a Nash equilibrium? \",current_pmg.is_equilibrium(sol_dict))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Game conversion\n",
    "We may also convert the game to a normal form game and try to solve it again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['A0' 'A1' 'A2' 'U0' 'U1' 'U2']\n",
      " ['0' '0' '0' '10' '13' '10']\n",
      " ['0' '0' '1' '10' '7' '13']\n",
      " ['0' '0' '2' '12' '8' '10']\n",
      " ['0' '1' '0' '4' '10' '11']\n",
      " ['0' '1' '1' '4' '6' '6']\n",
      " ['0' '1' '2' '6' '6' '8']\n",
      " ['0' '2' '0' '7' '13' '11']\n",
      " ['0' '2' '1' '7' '9' '6']\n",
      " ['0' '2' '2' '9' '8' '7']\n",
      " ['1' '0' '0' '12' '14' '15']\n",
      " ['1' '0' '1' '14' '8' '9']\n",
      " ['1' '0' '2' '9' '9' '11']\n",
      " ['1' '1' '0' '8' '15' '16']\n",
      " ['1' '1' '1' '10' '11' '2']\n",
      " ['1' '1' '2' '5' '11' '9']\n",
      " ['1' '2' '0' '14' '11' '16']\n",
      " ['1' '2' '1' '16' '7' '2']\n",
      " ['1' '2' '2' '11' '6' '8']\n",
      " ['2' '0' '0' '6' '13' '10']\n",
      " ['2' '0' '1' '6' '7' '9']\n",
      " ['2' '0' '2' '6' '8' '12']\n",
      " ['2' '1' '0' '10' '16' '11']\n",
      " ['2' '1' '1' '10' '12' '2']\n",
      " ['2' '1' '2' '10' '12' '10']\n",
      " ['2' '2' '0' '6' '12' '11']\n",
      " ['2' '2' '1' '6' '8' '2']\n",
      " ['2' '2' '2' '6' '7' '9']]\n",
      "CPU times: user 1.62 s, sys: 11.7 ms, total: 1.63 s\n",
      "Wall time: 1.63 s\n",
      "{0: [0, 0, 1.000000000000000?], 1: [0, 1.000000000000000?, 0], 2: [1.000000000000000?, 0, 0]}\n",
      "Player 0: [0, 0, 1]\n",
      "Player 1: [0, 1, 0]\n",
      "Player 2: [1, 0, 0]\n"
     ]
    }
   ],
   "source": [
    "converted_nfg = current_pmg.convert_to_NFG()\n",
    "print(converted_nfg)\n",
    "%time equilibrium = wilson(converted_nfg)\n",
    "\n",
    "print(equilibrium)\n",
    "\n",
    "for n in equilibrium.keys():\n",
    "    print(f\"Player {n}: {[x.radical_expression() for x in equilibrium[n]]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Hypergraphical games\n",
    "### 3.1 Definition of a hypergraphical game\n",
    "Definition of a hypergraphical game: Object, ascii representation...\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "C.H. Papadimitriou and T. Roughgarden. Computing correlated equilibria\n",
    "in multi-player games. Journal of the ACM 55, 3(14), 2008.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Utilities of the hypergraphical game:\n",
      "Local game  0 : \n",
      " [['A0' 'A1' 'A3' 'U0' 'U1' 'U3']\n",
      " ['0' '0' '0' '74' '51' '18']\n",
      " ['0' '0' '1' '6' '97' '16']\n",
      " ['0' '1' '0' '9' '98' '29']\n",
      " ['0' '1' '1' '60' '83' '86']\n",
      " ['1' '0' '0' '77' '90' '100']\n",
      " ['1' '0' '1' '30' '1' '96']\n",
      " ['1' '1' '0' '47' '2' '93']\n",
      " ['1' '1' '1' '0' '47' '64']]\n",
      "Local game  1 : \n",
      " [['A0' 'A2' 'A3' 'U0' 'U2' 'U3']\n",
      " ['0' '0' '0' '84' '10' '1']\n",
      " ['0' '0' '1' '48' '88' '36']\n",
      " ['0' '1' '0' '27' '72' '68']\n",
      " ['0' '1' '1' '5' '47' '0']\n",
      " ['1' '0' '0' '100' '13' '53']\n",
      " ['1' '0' '1' '62' '24' '5']\n",
      " ['1' '1' '0' '16' '42' '61']\n",
      " ['1' '1' '1' '62' '72' '97']]\n",
      "\n",
      " Hypergraph: \n",
      "[[0, 1, 3], [0, 2, 3]]\n"
     ]
    }
   ],
   "source": [
    "from gtnash.game.hypergraphicalgame import *\n",
    "utilities = [\n",
    "    [[74, 6, 9, 60, 77, 30, 47, 0], [51, 97, 98, 83, 90, 1, 2, 47],\n",
    "    [18, 16, 29, 86, 100, 96, 93, 64]],\n",
    "    [[84, 48, 27, 5, 100, 62, 16, 62], [10, 88, 72, 47, 13, 24, 42, 72],\n",
    "    [1, 36, 68, 0, 53, 5, 61, 97]]]\n",
    "hypergraph = [[0, 1, 3], [0, 2, 3]]\n",
    "players_actions = [[0, 1], [0, 1], [0, 1], [0, 1]]\n",
    "current_hgg = HGG(players_actions, utilities, hypergraph)\n",
    "print(\"Utilities of the hypergraphical game:\")\n",
    "print(current_hgg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Solving a hypergraphical game, using Wilson's algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We solve the hypergraphical game, using Wilson's algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 6.87 s, sys: 10.3 ms, total: 6.88 s\n",
      "Wall time: 6.89 s\n",
      "{0: [0.651851851851852?, 0.3481481481481481?], 1: [0.7714285714285714?, 0.2285714285714286?], 2: [0, 1.000000000000000?], 3: [1.000000000000000?, 0]}\n",
      "Is this an equilibrium?  True\n",
      "Algebraic number representation:\n",
      "Player 0: [88/135, 47/135]\n",
      "Player 1: [27/35, 8/35]\n",
      "Player 2: [0, 1]\n",
      "Player 3: [1, 0]\n"
     ]
    }
   ],
   "source": [
    "%time equilibrium = wilson(current_hgg)\n",
    "print(equilibrium)\n",
    "print(\"Is this an equilibrium? \",current_hgg.is_equilibrium(equilibrium))\n",
    "print(\"Algebraic number representation:\")\n",
    "for n in equilibrium.keys():\n",
    "    print(f\"Player {n}: {[x.radical_expression() for x in equilibrium[n]]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the PNS algorithm on the normal form game conversion of the hypergraphical game is faster and returns a different equilibrium:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 420 ms, sys: 8.15 ms, total: 428 ms\n",
      "Wall time: 426 ms\n",
      "{0: [0.7666666666666667?, 0.2333333333333334?], 1: [0.5476190476190476?, 0.4523809523809524?], 2: [1.000000000000000?, 0], 3: [0, 1.000000000000000?]}\n",
      "Is this an equilibrium?  True\n",
      "Algebraic number representation:\n",
      "Player 0: [23/30, 7/30]\n",
      "Player 1: [23/42, 19/42]\n",
      "Player 2: [1, 0]\n",
      "Player 3: [0, 1]\n"
     ]
    }
   ],
   "source": [
    "converted_nfg = current_hgg.convert_to_NFG()\n",
    "%time equilibrium_PNS = PNS_solver(converted_nfg).launch_pns()\n",
    "print(equilibrium_PNS)\n",
    "print(\"Is this an equilibrium? \",current_hgg.is_equilibrium(equilibrium_PNS))\n",
    "print(\"Algebraic number representation:\")\n",
    "for n in equilibrium_PNS.keys():\n",
    "    print(f\"Player {n}: {[x.radical_expression() for x in equilibrium_PNS[n]]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But Wilson's algorithm on the converted NFG is also faster (Wilson's algorithm on hypergraphical games solves PCP of smaller degree, but with a larger number of variables):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 2.53 s, sys: 8.1 ms, total: 2.54 s\n",
      "Wall time: 2.54 s\n",
      "{0: [0.651851851851852?, 0.3481481481481481?], 1: [0.7714285714285714?, 0.2285714285714286?], 2: [0, 1.000000000000000?], 3: [1.000000000000000?, 0]}\n",
      "Is this an equilibrium?  True\n",
      "Algebraic number representation:\n",
      "Player 0: [88/135, 47/135]\n",
      "Player 1: [27/35, 8/35]\n",
      "Player 2: [0, 1]\n",
      "Player 3: [1, 0]\n"
     ]
    }
   ],
   "source": [
    "%time equilibrium_wilson = wilson(converted_nfg)\n",
    "print(equilibrium_wilson)\n",
    "print(\"Is this an equilibrium? \",current_hgg.is_equilibrium(equilibrium_wilson))\n",
    "print(\"Algebraic number representation:\")\n",
    "for n in equilibrium_wilson.keys():\n",
    "    print(f\"Player {n}: {[x.radical_expression() for x in equilibrium_wilson[n]]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Bayesian games and hypergraphical bayesian games\n",
    "Here are a few examples of Bayesian games definition and conversion to hypergraphical games.\n",
    "\n",
    "First, a bimatrix Bayesian game:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Theta 0 = [[0 0]] and P(Theta 0 ) = 1/10 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-3' '-3']\n",
      " ['0' '1' '-7' '-7']\n",
      " ['1' '0' '-10' '-1']\n",
      " ['1' '1' '-1' '-5']\n",
      " ['2' '0' '-5' '-10']\n",
      " ['2' '1' '-9' '-9']] \n",
      "\n",
      "Theta 1 = [[0 1]] and P(Theta 1 ) = 1/5 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-2' '-2']\n",
      " ['0' '1' '-6' '-6']\n",
      " ['1' '0' '-9' '-1']\n",
      " ['1' '1' '-1' '-4']\n",
      " ['2' '0' '-4' '-9']\n",
      " ['2' '1' '-8' '-8']] \n",
      "\n",
      "Theta 2 = [[0 2]] and P(Theta 2 ) = 1/5 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-2' '-2']\n",
      " ['0' '1' '-5' '-5']\n",
      " ['1' '0' '-8' '-1']\n",
      " ['1' '1' '-1' '-3']\n",
      " ['2' '0' '-3' '-8']\n",
      " ['2' '1' '-7' '-7']] \n",
      "\n",
      "Theta 3 = [[1 0]] and P(Theta 3 ) = 1/5 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-2' '-2']\n",
      " ['0' '1' '-4' '-4']\n",
      " ['1' '0' '-7' '-1']\n",
      " ['1' '1' '-1' '-3']\n",
      " ['2' '0' '-3' '-7']\n",
      " ['2' '1' '-6' '-6']] \n",
      "\n",
      "Theta 4 = [[1 1]] and P(Theta 4 ) = 1/10 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-2' '-2']\n",
      " ['0' '1' '-4' '-4']\n",
      " ['1' '0' '-6' '-1']\n",
      " ['1' '1' '-1' '-3']\n",
      " ['2' '0' '-3' '-6']\n",
      " ['2' '1' '-5' '-5']] \n",
      "\n",
      "Theta 5 = [[1 2]] and P(Theta 5 ) = 1/5 \n",
      " [['A0' 'A1' 'U0' 'U1']\n",
      " ['0' '0' '-3' '-3']\n",
      " ['0' '1' '-7' '-7']\n",
      " ['1' '0' '-1' '-10']\n",
      " ['1' '1' '-5' '-1']\n",
      " ['2' '0' '-10' '-5']\n",
      " ['2' '1' '-9' '-9']] \n",
      "\n",
      "\n",
      " Type: \n",
      "[[0, 1], [0, 1, 2]]\n"
     ]
    }
   ],
   "source": [
    "players_actions = [[0, 1, 2], [0, 1]]\n",
    "utilities = [[[-3, -7, -10, -1, -5, -9], [-3, -7, -1, -5, -10, -9]],\n",
    "            [[-2, -6, -9, -1, -4, -8], [-2, -6, -1, -4, -9, -8]],\n",
    "            [[-2, -5, -8, -1, -3, -7], [-2, -5, -1, -3, -8, -7]],\n",
    "            [[-2, -4, -7, -1, -3, -6], [-2, -4, -1, -3, -7, -6]],\n",
    "            [[-2, -4, -6, -1, -3, -5], [-2, -4, -1, -3, -6, -5]],\n",
    "            [[-3, -7, -1, -5, -10, -9], [-3, -7, -10, -1, -5, -9]]]\n",
    "theta = [[0, 1], [0, 1, 2]]\n",
    "p = [2, 4, 4, 4, 2, 4]\n",
    "bg = BG(players_actions, utilities, theta, p)\n",
    "print(bg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This bayesian bimatrix game can be converted into a polymatrix game:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Local game  0 : \n",
      " [['A0' 'A2' 'U0' 'U2']\n",
      " ['0' '0' '-3/5' '-1']\n",
      " ['0' '1' '-7/5' '-7/3']\n",
      " ['1' '0' '-2' '-1/3']\n",
      " ['1' '1' '-1/5' '-5/3']\n",
      " ['2' '0' '-1' '-10/3']\n",
      " ['2' '1' '-9/5' '-3']]\n",
      "Local game  1 : \n",
      " [['A0' 'A3' 'U0' 'U3']\n",
      " ['0' '0' '-4/5' '-4/3']\n",
      " ['0' '1' '-12/5' '-4']\n",
      " ['1' '0' '-18/5' '-2/3']\n",
      " ['1' '1' '-2/5' '-8/3']\n",
      " ['2' '0' '-8/5' '-6']\n",
      " ['2' '1' '-16/5' '-16/3']]\n",
      "Local game  2 : \n",
      " [['A0' 'A4' 'U0' 'U4']\n",
      " ['0' '0' '-4/5' '-1']\n",
      " ['0' '1' '-2' '-5/2']\n",
      " ['1' '0' '-16/5' '-1/2']\n",
      " ['1' '1' '-2/5' '-3/2']\n",
      " ['2' '0' '-6/5' '-4']\n",
      " ['2' '1' '-14/5' '-7/2']]\n",
      "Local game  3 : \n",
      " [['A1' 'A2' 'U1' 'U2']\n",
      " ['0' '0' '-4/5' '-4/3']\n",
      " ['0' '1' '-8/5' '-8/3']\n",
      " ['1' '0' '-14/5' '-2/3']\n",
      " ['1' '1' '-2/5' '-2']\n",
      " ['2' '0' '-6/5' '-14/3']\n",
      " ['2' '1' '-12/5' '-4']]\n",
      "Local game  4 : \n",
      " [['A1' 'A3' 'U1' 'U3']\n",
      " ['0' '0' '-2/5' '-2/3']\n",
      " ['0' '1' '-4/5' '-4/3']\n",
      " ['1' '0' '-6/5' '-1/3']\n",
      " ['1' '1' '-1/5' '-1']\n",
      " ['2' '0' '-3/5' '-2']\n",
      " ['2' '1' '-1' '-5/3']]\n",
      "Local game  5 : \n",
      " [['A1' 'A4' 'U1' 'U4']\n",
      " ['0' '0' '-6/5' '-3/2']\n",
      " ['0' '1' '-14/5' '-7/2']\n",
      " ['1' '0' '-2/5' '-5']\n",
      " ['1' '1' '-2' '-1/2']\n",
      " ['2' '0' '-4' '-5/2']\n",
      " ['2' '1' '-18/5' '-9/2']]\n",
      "\n",
      " Hypergraph: \n",
      "[[0, 2], [0, 3], [0, 4], [1, 2], [1, 3], [1, 4]]\n",
      "Correspondance between players in the transformed game and the (player,type) tuples of the bayesian game:\n",
      "{0: (0, 0), 1: (0, 1), 2: (1, 0), 3: (1, 1), 4: (1, 2)}\n",
      "Is the new game a polymatrix game?  True\n",
      "So, it can be solved using Lemke-Howson's algorithm:\n",
      "CPU times: user 1.84 ms, sys: 41 µs, total: 1.89 ms\n",
      "Wall time: 1.75 ms\n",
      "player 0 -> mixed strategy -> [1.0, 0.0, 0.0]\n",
      "player 1 -> mixed strategy -> [1.0, 0.0, 0.0]\n",
      "player 2 -> mixed strategy -> [1.0, 0.0]\n",
      "player 3 -> mixed strategy -> [1.0, 0.0]\n",
      "player 4 -> mixed strategy -> [1.0, 0.0]\n",
      "Is this really an equilibrium? True\n"
     ]
    }
   ],
   "source": [
    "hgg_from_bg,index_to_players = bg.convert_to_HGG()\n",
    "print(hgg_from_bg)\n",
    "print(\"Correspondance between players in the transformed game and the (player,type) tuples of the bayesian game:\")\n",
    "print(index_to_players)\n",
    "print(\"Is the new game a polymatrix game? \",hgg_from_bg.is_PMG())\n",
    "print(\"So, it can be solved using Lemke-Howson's algorithm:\")\n",
    "%time solver = LHpolymatrix(hgg_from_bg)\n",
    "omega0 = [1,1,1,1,1] # Initial joint action used to initialize the Lemke-Howson algorithm\n",
    "sol_dict = solver.launch_solver(omega0)\n",
    "for key in sol_dict.keys():\n",
    "    mixed_strat = [float(p) for p in sol_dict[key]]\n",
    "    print(f'player {key} -> mixed strategy -> {mixed_strat}')\n",
    "print(\"Is this really an equilibrium?\",hgg_from_bg.is_equilibrium(sol_dict))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us try a large bayesian hypergraphical game:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Bayesian game with 4 players, 2 types, 2 actions each and 2 local bayesian games.\n"
     ]
    }
   ],
   "source": [
    "from gtnash.game.bayesian_hypergraphicalgame import *\n",
    "\n",
    "players_actions = [[0, 1], [0, 1], [0, 1], [0, 1]]\n",
    "theta = [[0, 1], [0, 1], [0, 1], [0, 1]]\n",
    "utilities = [[[[39, 19, 95, 0, 32, 97, 65, 69],\n",
    "                   [22, 40, 67, 14, 100, 60, 5, 34],\n",
    "                   [46, 27, 72, 45, 18, 84, 100, 33]],\n",
    "                  [[61, 96, 22, 34, 34, 35, 39, 54],\n",
    "                   [39, 42, 77, 76, 27, 45, 56, 0],\n",
    "                   [21, 94, 80, 74, 100, 20, 55, 65]],\n",
    "                  [[34, 47, 56, 73, 55, 59, 31, 17],\n",
    "                   [57, 43, 0, 23, 41, 100, 73, 86],\n",
    "                   [24, 53, 95, 45, 38, 17, 28, 76]],\n",
    "                  [[47, 37, 100, 6, 20, 16, 17, 59],\n",
    "                   [40, 44, 64, 13, 0, 73, 76, 71],\n",
    "                   [23, 52, 19, 70, 41, 43, 30, 28]],\n",
    "                  [[53, 38, 44, 33, 9, 71, 31, 82],\n",
    "                   [62, 100, 70, 65, 74, 22, 47, 59],\n",
    "                   [66, 82, 20, 0, 15, 3, 57, 60]],\n",
    "                  [[46, 33, 0, 31, 50, 50, 35, 50],\n",
    "                   [26, 77, 56, 47, 37, 51, 56, 57],\n",
    "                   [28, 27, 100, 21, 29, 26, 83, 25]],\n",
    "                  [[70, 88, 0, 15, 7, 34, 100, 58],\n",
    "                   [53, 68, 17, 17, 16, 26, 84, 61],\n",
    "                   [63, 80, 10, 15, 11, 43, 84, 50]],\n",
    "                  [[91, 49, 99, 85, 62, 38, 21, 87],\n",
    "                   [18, 36, 74, 84, 40, 45, 39, 88],\n",
    "                   [63, 54, 57, 100, 45, 20, 0, 76]]],\n",
    "                 [[[72, 67, 47, 94, 73, 92, 80, 81],\n",
    "                   [55, 66, 45, 62, 78, 91, 55, 53],\n",
    "                   [54, 75, 47, 42, 0, 52, 47, 100]],\n",
    "                  [[64, 79, 39, 7, 43, 22, 68, 26],\n",
    "                   [48, 67, 62, 5, 74, 82, 77, 65],\n",
    "                   [40, 65, 38, 0, 59, 69, 100, 86]],\n",
    "                  [[44, 53, 46, 82, 44, 89, 69, 97],\n",
    "                   [61, 96, 88, 93, 70, 20, 100, 46],\n",
    "                   [31, 89, 0, 61, 48, 77, 84, 46]],\n",
    "                  [[59, 74, 63, 100, 34, 31, 38, 45],\n",
    "                   [55, 52, 83, 74, 10, 65, 5, 50],\n",
    "                   [74, 64, 65, 96, 19, 70, 0, 38]],\n",
    "                  [[32, 100, 0, 54, 5, 37, 45, 66],\n",
    "                   [2, 23, 49, 3, 62, 21, 23, 44],\n",
    "                   [11, 54, 67, 8, 37, 69, 83, 27]],\n",
    "                  [[41, 67, 72, 12, 65, 74, 65, 53],\n",
    "                   [68, 68, 57, 49, 0, 91, 80, 61],\n",
    "                   [97, 78, 86, 100, 34, 62, 57, 87]],\n",
    "                  [[52, 54, 63, 18, 37, 67, 75, 73],\n",
    "                   [47, 65, 93, 91, 59, 60, 0, 43],\n",
    "                   [82, 76, 49, 91, 100, 82, 95, 50]],\n",
    "                  [[39, 19, 95, 0, 32, 97, 65, 69],\n",
    "                   [22, 40, 67, 14, 100, 60, 5, 34],\n",
    "                   [46, 27, 72, 45, 18, 84, 100, 33]]]]\n",
    "p = [[6, 9, 8, 6, 6, 4, 3, 3], [1, 7, 9, 1, 7, 3, 6, 9]]\n",
    "hypergraph = [[0, 1, 2], [0, 2, 3]]\n",
    "bhgg = BHGG(players_actions, utilities, hypergraph, theta, p)\n",
    "\n",
    "print(f\"Bayesian game with {bhgg.n_players} players, {len(bhgg.theta[0])} types, \\\n",
    "{len(bhgg.players_actions[0])} actions each and {len(bhgg.hypergraph)} local bayesian games.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can solve it, through first transforming it into a hypergraphical game and then into a normal form game:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "We can compute first a:\n",
      "Hypergraphical game with 8 players, 2 actions each and 16 local games of size 3.\n",
      "Then, we compute a normal form game from this HGG.\n",
      "We solve it using the PNS solver:\n",
      "CPU times: user 11.4 s, sys: 487 ms, total: 11.9 s\n",
      "Wall time: 12 s\n",
      "The corresponding equilibrium is:\n",
      "{0: [1, 0], 1: [0, 1], 2: [0, 1], 3: [1, 0], 4: [1, 0], 5: [1, 0], 6: [0, 1], 7: [0, 1]}\n",
      "Solving the same NFG using Wilson's algorithm takes far more time, so we do not do it\n"
     ]
    }
   ],
   "source": [
    "hgg_from_bhgg,index_to_players = bhgg.convert_to_HGG()\n",
    "print(\"We can compute first a:\")\n",
    "print(f\"Hypergraphical game with {hgg_from_bhgg.n_players} players, {len(hgg_from_bhgg.players_actions[0])} \\\n",
    "actions each and {len(hgg_from_bhgg.hypergraph)} local games of size {len(hgg_from_bhgg.hypergraph[0])}.\")\n",
    "\n",
    "nfg_from_bhgg = hgg_from_bhgg.convert_to_NFG()\n",
    "print(\"Then, we compute a normal form game from this HGG.\")\n",
    "\n",
    "\n",
    "print(\"We solve it using the PNS solver:\")\n",
    "%time equilibrium_PNS = PNS_solver(nfg_from_bhgg).launch_pns()\n",
    "print(\"The corresponding equilibrium is:\")\n",
    "print(equilibrium_PNS)\n",
    "print(\"Solving the same NFG using Wilson's algorithm takes far more time, so we do not do it\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try a smaller BHGG (indeed, a Bayesian polymatrix game):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Bayesian game with 3 players, 2 types, 2 actions each and 2 local bayesian games.\n",
      "We can compute first a:\n",
      "Hypergraphical game with 6 players, 2 actions each and 8 local games of size 2.\n",
      "We can solve the HGG using Lemke and Howson's algorithm, since it is a polymatrix game:\n"
     ]
    }
   ],
   "source": [
    "players_actions = [[0, 1], [0, 1], [0, 1]]\n",
    "theta = [[0, 1], [0, 1], [0, 1]]\n",
    "hypergraph = [[1, 2], [0, 1]]\n",
    "p = [[Fraction(7, 19), Fraction(1, 19), Fraction(2, 19), Fraction(9, 19)],\n",
    "      [Fraction(1, 17), Fraction(4, 17), Fraction(5, 17), Fraction(7, 17)]]\n",
    "utilities = [[[[15, 41, 0, 7], [14, 23, 65, 100]],\n",
    "               [[0, 41, 58, 92], [100, 51, 72, 93]],\n",
    "               [[56, 38, 0, 14], [100, 95, 43, 0]],\n",
    "               [[53, 69, 17, 15], [98, 12, 100, 0]]],\n",
    "               [[[39, 12, 42, 50], [0, 78, 38, 100]],\n",
    "               [[0, 7, 100, 58], [15, 10, 66, 78]],\n",
    "               [[74, 100, 96, 76], [0, 29, 98, 35]],\n",
    "               [[52, 21, 10, 100], [0, 43, 81, 56]]]]\n",
    "\n",
    "bhgg = BHGG(players_actions, utilities, hypergraph, theta, p)\n",
    "\n",
    "print(f\"Bayesian game with {bhgg.n_players} players, {len(bhgg.theta[0])} types, \\\n",
    "{len(bhgg.players_actions[0])} actions each and {len(bhgg.hypergraph)} local bayesian games.\")\n",
    "\n",
    "hgg_from_bhgg,index_to_players = bhgg.convert_to_HGG()\n",
    "print(\"We can compute first a:\")\n",
    "print(f\"Hypergraphical game with {hgg_from_bhgg.n_players} players, {len(hgg_from_bhgg.players_actions[0])} \\\n",
    "actions each and {len(hgg_from_bhgg.hypergraph)} local games of size {len(hgg_from_bhgg.hypergraph[0])}.\")\n",
    "\n",
    "print(\"We can solve the HGG using Lemke and Howson's algorithm, since it is a polymatrix game:\")\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Is the obtained hypergraphical game a polymatrix game?\n",
      "True\n",
      "So, we can solve it using the Lemke-Howson algorithm:\n",
      "CPU times: user 4.9 ms, sys: 39 µs, total: 4.94 ms\n",
      "Wall time: 4.28 ms\n",
      "player 0 -> mixed strategy -> [0.0, 1.0]\n",
      "player 1 -> mixed strategy -> [1.0, 0.0]\n",
      "player 2 -> mixed strategy -> [0.0, 1.0]\n",
      "player 3 -> mixed strategy -> [1.0, 0.0]\n",
      "player 4 -> mixed strategy -> [0.0, 1.0]\n",
      "player 5 -> mixed strategy -> [1.0, 0.0]\n",
      "Is this an equilibrium?  True\n",
      "In order to solve it using Wilson's algorithm, we first transform it into a normal-form game:\n",
      "We first solve it using the PNS solver:\n",
      "CPU times: user 589 ms, sys: 23.8 ms, total: 613 ms\n",
      "Wall time: 615 ms\n",
      "{0: [0, 1], 1: [1, 0], 2: [0, 1], 3: [1, 0], 4: [0, 1], 5: [1, 0]}\n",
      "Is this an equilibrium?  True\n",
      "Then, we solve it using Wilson's algorithm:\n",
      "CPU times: user 1.67 s, sys: 7.81 ms, total: 1.68 s\n",
      "Wall time: 1.67 s\n",
      "Here is the Nash equilibrium:  {0: [0, 1], 1: [1, 0], 2: [0, 1], 3: [1, 0], 4: [0, 1], 5: [1, 0]}\n"
     ]
    }
   ],
   "source": [
    "print(\"Is the obtained hypergraphical game a polymatrix game?\")\n",
    "print(hgg_from_bhgg.is_PMG())\n",
    "\n",
    "print(\"So, we can solve it using the Lemke-Howson algorithm:\")\n",
    "\n",
    "omega0 = [0,0,0,0,0,0]\n",
    "%time sol_dict = LHpolymatrix(hgg_from_bhgg).launch_solver(omega0)\n",
    "for key in sol_dict.keys():\n",
    "    mixed_strat = [float(p) for p in sol_dict[key]]\n",
    "    print(f'player {key} -> mixed strategy -> {mixed_strat}')\n",
    "\n",
    "print(\"Is this an equilibrium? \",hgg_from_bhgg.is_equilibrium(sol_dict))\n",
    "\n",
    "print(\"In order to solve it using Wilson's algorithm, we first transform it into a normal-form game:\")\n",
    "nfg_from_bhgg = bhgg.convert_to_NFG()\n",
    "print(\"We first solve it using the PNS solver:\")\n",
    "%time equilibrium_PNS = PNS_solver(nfg_from_bhgg).launch_pns()\n",
    "print(equilibrium_PNS)\n",
    "print(\"Is this an equilibrium? \",nfg_from_bhgg.is_equilibrium(equilibrium_PNS))\n",
    "\n",
    "#print(nfg_from_bhgg)\n",
    "print(\"Then, we solve it using Wilson's algorithm:\")\n",
    "%time equilibrium_wilson_bhgg = wilson(nfg_from_bhgg) \n",
    "\n",
    "print(\"Here is the Nash equilibrium: \",equilibrium_wilson_bhgg)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Game files formats\n",
    "There are several methods for reading and writing normal-form games as ASCII files in Gambit format. \n",
    "We have also defined file formats for hypergraphical games, polymatrix games, bayesian games, etc. \n",
    "See the documentation of these functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
