Utilities
=========

Some functions useful in the different solvers:

* IRDA: Used to simplify the game a priori, before solving it.
* PCP: USed in both Wilson and PNS algorith, to build and solve Polynomial Complementarity Problems

Iterated Removal of Dominated Alternatives
------------------------------------------

.. automodule:: irda
   :members:

Polynomial Complementarity Problems
-----------------------------------

.. automodule:: polynomial_complementary_problem
   :members:

