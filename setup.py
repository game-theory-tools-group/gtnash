import setuptools

setuptools.setup(
    name="gtnash",
    description="Python Nash Equilibrium Computation Utilities",
    version="1.0.0",
    author="Régis Sabbadin",
    author_email="regis.sabbadin@inrae.fr",
    url="https://forgemia.inra.fr/game-theory-tools-group/gtnash/",
    packages=setuptools.find_packages()
)
