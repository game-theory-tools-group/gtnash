# Using container environment

Prerequisites : 
* https://docs.docker.com/engine/install/
* https://docs.docker.com/engine/security/rootless/

Using base image sagemath/sagemath:9.0-py3 : https://hub.docker.com/layers/sagemath/sagemath/9.0-py3/images/sha256-65ba865735ee58772d7b7843b4c37bdcd45a97dc1bf958c054cb9d4661d23013?context=explore

## Local building of the container 
```bash
docker build --tag gtnash --file docker/Dockerfile .

docker build --build-arg REF_BASE_IMAGE=registry.forgemia.inra.fr/algebraic-nash/nash-eq-wilson/sagemath:9.0-py3 --build-arg BUILDKIT_INLINE_CACHE=1 --tag registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest --file docker/Dockerfile .
```

## Get/Update image from the remote container registry 
```bash
docker login registry.forgemia.inra.fr

docker pull registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest
```

## Running a container 
```bash
docker run --rm -it registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest

docker run --rm -it registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest bash

docker run --rm -it -v "$(pwd):/mnt" -w /mnt -u root registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest bash

docker run --rm -it -v "$(pwd):/mnt" -w /mnt -u root -p 8888:8888 registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest bash
```

Example commands from within an interactive container
```bash
flake8

autopep8 --in-place --recursive .

cd /mnt/test && pytest && cd /mnt

sage -pip install -e .

sage -n jupyter --allow-root --no-browse --ip=0.0.0.0 --port=8888
```


## Running a container to use flake8, pytest and serve jupyter on local source code

```bash
docker run --rm -it -v "$(pwd):/mnt" -w /mnt -u root registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest flake8

docker run --rm -it -v "$(pwd):/mnt" -w /mnt/test -u root registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest pytest

docker run --rm -it -v "$(pwd):/mnt" -w /mnt -p 8888:8888 -u root registry.forgemia.inra.fr/game-theory-tools-group/gtnash:latest sage -n jupyter --allow-root --no-browse --ip=0.0.0.0 --port=8888
```

